import { Controller } from 'stimulus'

export default class ExistenceController extends Controller {
  connect() {
    this.dispatch('added')
  }

  remove() {
    this.dispatch('removed')
    this.element.remove()
  }
}
